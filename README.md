Butterfingers is a command line tool that creates periodic
snapshots of designated working directories.
It is aimed at users that frequently work with the command
line and suffer from occasional butterfingers syndrome.
If a mistake is made and the files are damaged, e.g., a mass
renaming gone wrong, then they can be easily restored from
a previous snapshot by using the restore function.

A directory can be easily added to the watchlist. After a certain
amount of inactivity in an ephemeral directory, it will be
automatically removed from the watchlist and its snapshots deleted.
Therefore, the user does not need to worry that the snapshots may
fill up the disk.
If it is desired to monitor a directory indefinitely, then it
has to be marked as non-ephemeral. In this case, too, the disk
is not going to fill up indefinitely because older snapshots
are automatically deleted after a certain amount of time.
However, in a non-ephemeral directory, the latest snapshot
will never be deleted, regardless how much time of inactivity
has passed.

Butterfingers is published under the GPL3 license. You should
have received a copy of the license.

