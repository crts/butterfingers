## [1.0] 2021-03-04
### Added
- Started this CHANGELOG
- Licence notice

### Fixed
- Fixed bug that breaks butterfingers for directories
  which contain blanks

### Changed
- Butterfingers is no longer part of arctools.

