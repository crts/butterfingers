#!/usr/bin/bash

declare -r basedir="${0%/*}"

declare -a parts=( "$@" )

all_tests() {
	exec 9< <(find "${1}" -mindepth 1 -maxdepth 1 -type d -name '[[:digit:]]*' -print | sort)
}

if [[ ${#parts[@]} == 0 ]];then
	parts=( "$basedir"/[[:digit:]]* )
fi

run() {
	for p in "${parts[@]}";do
		all_tests "$p"
		while read -u9 test;do
			"$basedir/test" $test < /dev/null
		done
	done
}

run

exit 0



