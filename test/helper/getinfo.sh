#!/bin/bash

declare -r basedir="${0%/*}"

declare -r dir="$1"
#info0="%p %TY-%Tm-%Td_%TH:%TM:%TS\n"
#info1="%p %TY-%Tm-%Td_%TH:%TM\n"
#info2="%p %TY\n"
info3="%TY-%Tm-%Td_%TH:%TM %i %p"

info4="%Ty-%Tm-%Td_%TH:%TM:%.2TS %i %P"

exclude="${2+-name $2 -prune -o}"
info="$info4"

declare p

while read -r -d $'\0' mod inode name;do
	echo -n "$mod $inode $name"

	p="$dir/$name"

	if [[ -d "$p" ]];then
		echo "/"
	else
		echo
		while read line;do
			echo "	|$line"
		done < "$p"
	fi
done < <(find "$dir" $exclude -printf "$info\0")


